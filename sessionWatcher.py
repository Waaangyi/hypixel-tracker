import os
import pymongo
from bson import json_util
import logging
from hypixel import Hypixel
import time
import copy


class SessionWatcher:
    def __init__(self, name, apikey):

        self.player = Hypixel(apikey)
        self.player.setPlayerName(name)

        # Establish connection with mongodb

        self.client = pymongo.MongoClient("mongodb://localhost:27017")

        self.db = self.client["HypixelTracker"]

        self.SessionStatus = self.db["SessionStatus_" + name]

    def pulse(self):
        statusData = self.player.getStatus()
        if (statusData == None):
            return None
        if self.findLast() == statusData:
            return None
        data = {"Timestamp": int(time.time()), "json": statusData}
        self.SessionStatus.insert_one(data)
        return data

    def findLast(self):
        try:
            return self.SessionStatus.find({}).sort(
                "Timestamp", -1).limit(1)[0]["json"]
        except:
            logging.error("Some error occured in SessionStatus Database")
            return None
