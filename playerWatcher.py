import os
import pymongo
from bson import json_util
import logging
from hypixel import Hypixel
import time
import copy


class PlayerWatcher:
    def __init__(self, name, apikey):

        self.player = Hypixel(apikey)
        self.player.setPlayerName(name)

        # Establish connection with mongodb

        self.client = pymongo.MongoClient("mongodb://localhost:27017")

        self.db = self.client["HypixelTracker"]

        self.PlayerStats = self.db["PlayerStats_" + name]

    def pulse(self):
        playerData = self.player.getPlayer()
        if (playerData == None):
            return None
        if self.findLast() == playerData:
            return None
        data = {"Timestamp": int(time.time()), "json": playerData}
        self.PlayerStats.insert_one(data)
        return data

    def findLast(self):
        try:
            return self.PlayerStats.find({}).sort(
                "Timestamp", -1).limit(1)[0]["json"]
        except:
            logging.error("Some error occured in PlayerStats Database")
            return None
