import os
import pymongo
from bson import json_util
import logging
from hypixel import Hypixel
import time
import copy


class GameWatcher:
    def __init__(self, name, apikey):

        self.player = Hypixel(apikey)
        self.player.setPlayerName(name)

        # Establish connection with mongodb

        self.client = pymongo.MongoClient("mongodb://localhost:27017")

        self.db = self.client["HypixelTracker"]

        self.GameStatus = self.db["GameStatus_" + name]
        self.oldData = None

    def pulse(self):
        gameData = self.player.getRecentGames()
        if gameData == None:
            return None


        if len(gameData) == 0:
            return None

        while len(gameData) != 0 and not "ended" in gameData[0]:
            gameData.pop(0)

        oldData = self.findLast()
        try:
            i = gameData.index(oldData)
            while len(gameData) != i:
                gameData.pop(i)
        except ValueError:
            pass

        # Store to database
        for i in reversed(gameData):
            data = {"Timestamp": i["date"], "json": i}
            self.GameStatus.insert_one(data)
        return gameData

    def findLast(self):
        try:
            return self.GameStatus.find({}).sort("Timestamp", -1).limit(1)[0]["json"]
        except Exception as e:
            logging.error("Some error occured in GameStatus Database" + str(e))
            return None
